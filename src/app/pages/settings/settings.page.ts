import { Component, ViewChild, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// import { UsersService } from '../../services/users.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserSettingDialog } from "../../../components/modals/userSetting/userSetting";

@Component({
  selector: 'settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss']
})
export class SettingsPage {
  
  user: any = {}

  constructor(public dialog: MatDialog,){}

  ngOnInit() {
    console.log("init")
    this.user = JSON.parse(localStorage.getItem('user'))
    console.log("this.user", this.user)
  }
  openDialog(type) {
    const dialogRef = this.dialog.open(UserSettingDialog, {
      maxWidth: "600px",
      minWidth: "300px",
      data: {
        type: type,
        user: this.user
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
