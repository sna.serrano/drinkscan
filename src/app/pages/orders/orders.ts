import { Component, ViewChild, HostListener } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store'
import { CREATE_ORDER, SELECT_ORDER } from '../../../redux/order'
import {animate, state, style, transition, trigger} from '@angular/animations';

import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { arraysAreNotAllowedMsg } from '@ngrx/store/src/models';

const waiting = [
  {table: 1, name: 'Simon Serrano', date: '18h36', items: [{name : "Biere", qty: 1, price: '7,5'}, {name : "Vin rouge", qty: 1, price: '5,5'}], price: '12€50', expanded: false, active: false},
  {table: 12, name: 'Steve Averty', date: '18h38', items: [{name : "Biere", qty: 1, price: '7,5'}, {name : "Vin rouge", qty: 1, price: '5,5'}, {name : "Café", qty: 1, price: '1,5'}], price: '14€', expanded: false, active: true},
];
const inProgress = [
  {table: 1, name: 'Simon Serrano', date: '18h36', items: [{name : "Biere", qty: 1, price: '7,5'}, {name : "Vin rouge", qty: 1, price: '5,5'}], price: '16€50', expanded: false, active: false},
  {table: 12, name: 'Steve Averty', date: '18h38', items: [{name : "Café", qty: 3, price: '1,5'}], price: '4,00€', expanded: false, active: false},
  {table: 24, name: 'Romain Bourée', date: '18h15', items: [{name : "Biere", qty: 1, price: '7,5'}, {name : "Vin rouge", qty: 1, price: '5,5'}, "1 Girafe"], price: '14€', expanded: false, active: false},
];
const history = [
  {table: 6, name: 'Valentine Jappain', date: '13h20', items: [{name : "Biere", qty: 1, price: '7,5'}, {name : "Vin rouge", qty: 1, price: '5,5'}, {name : "Biere", qty: 1, price: '7,5'}], price: '16€50', expanded: false, active: false},
  {table: 9, name: 'Julien Samet', date: '16h43', items: [{name : "Café", qty: 3, price: '1,5'}], price: '4,00€', expanded: false, active: false},
  {table: 16, name: 'Nattan Serrano', date: '18h15', items: [{name : "Biere", qty: 1, price: '7,5'}, {name : "Vin rouge", qty: 1, price: '5,5'}, {name : "Bière", qty: 1, price: '5,5'}], price: '14€', expanded: false, active: false},
];

@Component({
  selector: 'orders',
  templateUrl: 'orders.html',
  styleUrls: ['./orders.scss'],  
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', visibility: 'hidden', overflow: 'hidden'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class OrdersPage {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private store: Store<any>,) {
  }
  public expanded 
  public list 
  public tabList: Array<any> = [{tab: "waiting", name: "En attentes"}, {tab: "inProgress", name: "En cours"}, {tab: "history", name: "Historique"}]
  dataSource: MatTableDataSource<any>;
  dataSourceInProgress: MatTableDataSource<any>;
  dataSourceHistory: MatTableDataSource<any>;
  // dataSourceCont  = [];
  displayedColumns  : any = ['table', 'date', 'items', 'price', 'action'];
  ngOnInit() {
    this.createTable()
    this.store.select('orders').subscribe(orders => this.list = orders);
    this.route.params.subscribe( params => {
        switch (params['id']) {
          case 'waiting':
            this.list = waiting
            // this.store.dispatch({ type: CREATE_SIGNATURE, payload: response.data });
            break;
          
          case 'inProgress':
            this.list = inProgress
            break;
          
          case 'history':
            this.list = history
            break;
        
          default:
            break;
        }
    });
    console.log('list', this.list)
  }
  isSticky: boolean = false;

  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    this.isSticky = window.pageYOffset >= 78;
  }
  createTable() {
    this.dataSource = new MatTableDataSource(waiting);
    this.dataSourceInProgress = new MatTableDataSource(inProgress);
    this.dataSourceHistory = new MatTableDataSource(history);
  }
  getTab($event) {
    var element = document.getElementById($event.tab);
    element.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
  }

  acceptOrder(order) {
    const index = waiting.indexOf(order);
    inProgress.push(order)
    waiting.splice(index, 1)
    console.log('index', index)
  }
}
