import { Component, ViewChild, HostListener } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { arraysAreNotAllowedMsg } from '@ngrx/store/src/models';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { UpdateMenuItemDialog } from "../../../components/modals/updateMenuItem/updateMenuItem";
import {animate, state, style, transition, trigger} from '@angular/animations';

const beers = [
  {img: 'Img', name: 'Barbare Brune', description: 'Bière d\'abbaye', size: ["Pinte", "Demi", "Galopin"], price: ["7", "4", "2"]},
  {img: 'Img', name: 'Barbare Blonde', description: 'Bière d\'abbaye', size: ["Pinte", "Demi", "Galopin"], price: ["7", "4", "2"]},
  {img: 'Img', name: 'Guinness', description: 'Bière stout', size: ["Pinte"], price: ["7"]},
];
const wine = [
  {img: 'Img', name: 'Pinot noir d\'alsace', description: 'Vin rouge alsacien', size: ["Verre", "Bouteille"], price: ["7", "18"]},
  {img: 'Img', name: 'Piquette du coin', description: 'Vin acheté au SDF du bas de la rue', size: ["Verre"], price: ["2"]},
];
const softs = [
  {img: 'Img', name: 'Coca-Cola', description: 'Boisson sucré à la caféine', size: ["Verre"], price: ["2.5"]},
  {img: 'Img', name: 'Orangina', description: 'Boisson sucré à l\'orange', size: ["Verre"], price: ["2.5"]},
];


@Component({
  selector: 'menuEdit',
  templateUrl: 'menuEdit.html',
  styleUrls: ['./menuEdit.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', visibility: 'hidden', overflow: 'hidden'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class MenuEditPage {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private snackBar: MatSnackBar,
    private dialog: MatDialog,) {
  }
  public tabList: Array<any> = [{tab: "all", name: "Tout le menu"}, {tab: "beer", name: "Bières"}, {tab: "wine", name: "Vins"}, {tab: "coktails", name: "Coktails"}, {tab: "soft", name: "Softs"}, {tab: "hotDrink", name: "Boisson Chaude"}, {tab: "plates", name: "Plateaux"}]
  public dataSource: MatTableDataSource<any>;
  public dataSourceInProgress: MatTableDataSource<any>;
  public dataSourceHistory: MatTableDataSource<any>;
  public displayedColumns  : any = ['img', 'name', 'size', 'price', 'action'];

  public newItem: any = {}
  public options = ["Galopin", "Demi", "Pinte", "Girafe", "Verre", "Bouteille"];
  public chipControl = new FormControl(new Set());
  // isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  expandedElement: any;
  public allDrink = []
  ngOnInit() {
    let temp = beers.concat(wine);
    this.allDrink = temp.concat(softs);
    this.createTable()
  }
  
  isSticky: boolean = false;
  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    this.isSticky = window.pageYOffset >= 317;
  }

  createTable() {
    this.dataSource = new MatTableDataSource(this.allDrink);
  }
  recreateTable(table) {
    switch (table) {
      case 1:
        this.dataSource = new MatTableDataSource(beers);
        break;
      case 2:
        this.dataSource = new MatTableDataSource(wine);
        break;
      case 4:
        this.dataSource = new MatTableDataSource(softs);
        break;
        
      default:
        this.dataSource = new MatTableDataSource(this.allDrink);
        break;
    }
  }

  getTab($event) {
    console.log('ev', $event)
    this.recreateTable($event.index)

  }

  toggleChip = (chip: any) => {
    const addChip = () => { this.chips.add(chip); };
    const removeChip = () => { this.chips.delete(chip); };

    this.chips.has(chip) ? removeChip() : addChip();
  }
  get chips() { return this.chipControl.value; }

  create() {
    this.newItem.size = this.chips
    console.log('item', this.newItem)
    this.chipControl = new FormControl(new Set());
  }

  updateItem(item) {

    const dialogRef = this.dialog.open(UpdateMenuItemDialog, {
      maxWidth: '70vw',
      data: {
        item: item, 
      }
    });
  
    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
