import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// ***** Redux *****
import { StoreModule } from '@ngrx/store'
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducers, INITIAL_STATE } from '../redux'

// ***** App *****
import { SharedModule } from './shared.module';
import { ServiceModule } from '../services/services.module';

// ***** Components *****
import { ModalModule } from "../components/modals/modal.module";
import { TabsComponent } from "../components/tabs/tabs";

// ***** LandingPages *****
import { LandingProPage } from './landingPro/landingPro';
import { HomeLandingProPage } from './landingPro/home/home-landingPro';
import { PricingLandingProPage } from './landingPro/pricing/pricing-landingPro';

// ***** Pages *****
import { PagesComponent } from './pages/pages';
import { NotFoundPage } from './notFound/notFound.page';
import { LoginPage } from './login/login.page';
import { HomePage } from './pages/home/home';
import { OrdersPage } from './pages/orders/orders';
import { MenuEditPage } from './pages/menuEdit/menuEdit';
import { SettingsPage } from './pages/settings/settings.page';

@NgModule({
  declarations: [
    AppComponent,
    PagesComponent,
    NotFoundPage,
    LoginPage,
    HomePage,
    OrdersPage,
    TabsComponent,
    MenuEditPage,
    SettingsPage,
    LandingProPage,
    HomeLandingProPage,
    PricingLandingProPage,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ModalModule,
    SharedModule,
    ServiceModule,
    StoreModule.forRoot(reducers, { initialState: INITIAL_STATE }),
    StoreDevtoolsModule.instrument({ maxAge: 10, name: '[DRINKSCAN]' }),
    
  ],
  exports: [TabsComponent,],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
