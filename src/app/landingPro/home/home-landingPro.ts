import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'home-landingPro',
  templateUrl: 'home-landingPro.html',
  styleUrls: ['./home-landingPro.scss']
})
export class HomeLandingProPage {
  constructor(
    private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    document.body.style.overflow = "scroll"
  }
  ngOnDestroy() {
    document.body.style.overflow = "initial"
  }
 
}
