import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'landingPro',
  templateUrl: 'landingPro.html',
  styleUrls: ['./landingPro.scss']
})
export class LandingProPage {
  constructor(
    private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    document.body.style.backgroundColor = "#FFF"
  }
  ngOnDestroy() {
    document.body.style.backgroundColor = "initial"
  }
 
}
