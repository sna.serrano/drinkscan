import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/****** SERVICES ******/
import { AuthGuardService as AuthGuard } from "../services/auth-guard.service";

/****** LANDINGPAGES ******/
import { LandingProPage } from './landingPro/landingPro'
import { HomeLandingProPage } from './landingPro/home/home-landingPro'
import { PricingLandingProPage } from './landingPro/pricing/pricing-landingPro'

/****** PAGES ******/
import { HomePage } from './pages/home/home'
import { PagesComponent } from './pages/pages'
import { NotFoundPage } from './notFound/notFound.page'
import { LoginPage } from './login/login.page'
import { OrdersPage } from './pages/orders/orders'
import { MenuEditPage } from './pages/menuEdit/menuEdit'
import { SettingsPage } from './pages/settings/settings.page'


const routes: Routes = [
  { path: 'login', component: LoginPage },
  { path: '404', component: NotFoundPage },
  { path: '', component: LandingProPage,
    children: [
      { path: 'home', component: HomeLandingProPage },
      { path: 'pricing', component: PricingLandingProPage },
    ]
  },
  { path: 'app', component: PagesComponent,
    children: [
      { path: 'home', component: HomePage },
      { path: 'orders/:id', component: OrdersPage },
      // { path: 'orders/waiting', component: OrdersPage },
      // { path: 'orders/inProgress', component: OrdersPage },
      // { path: 'orders/history', component: OrdersPage },
      { path: 'menu', component: MenuEditPage },
      { path: 'settings', component: SettingsPage },
    ]
  },
  { path: '**', redirectTo: '404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
