// ***** Actions *****
export const CREATE_ORDER = 'CREATE_ORDER';
export const SELECT_ORDER = 'SELECT_ORDER';
// {table: 1, 
//     name: 'Simon Serrano', 
//     date: '18h36', 
//     items: [
//         {name : "Biere", 
//         qty: 1, 
//         price: '7,5'}, 
//         {name : "Vin rouge", qty: 1, price: '5,5'}], 
//         price: '12€50', 
//         expanded: false, 
//         active: false},
// ***** Order *****
export interface Order {
    id: string;
    table: string;
    customer: string;
    orderDate: string;
    totalPrice: string;
    status: string;
    items: Array<any>;
}
// ***** Initial data *****
export const orderInitial: Order = {
    id: null,
    table: null,
    customer: null,
    orderDate: null,
    totalPrice: null,
    status: null,
    items: [],
}

// ***** Reducer *****
export function orderReducer(state: Order, action): Order {
    switch (action.type) {

        case CREATE_ORDER:
            // action.payload.orderDate = new Date();
            return action.payload

        case SELECT_ORDER:
            return action.payload

        default: return state;
    }
}
