import { userReducer, userInitial } from './user'
import { orderReducer, orderInitial } from './order'

export const reducers = {
  user: userReducer,
  order: orderReducer,
}

export const INITIAL_STATE = {
  user: userInitial,
  order: orderInitial,
}
