import { Component, Input, Output, Inject, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: 'tabs',
  templateUrl: 'tabs.html',
  styleUrls: ['./tabs.scss']
})
export class TabsComponent {
  constructor(
    private snackBar: MatSnackBar) {
  }

  @Output() tabEmit = new EventEmitter<any>();

  @Input() tabList: any[];

  public tabSelected

  ngOnInit() {
    this.tabSelected = this.tabList[0].tab
  }

  clickTab(tab) {
    this.tabSelected = tab.tab
    return this.tabEmit.emit(tab);
  }
}