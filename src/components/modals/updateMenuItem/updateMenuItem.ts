import { Component, Input, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: 'updateMenuItem',
  templateUrl: 'updateMenuItem.html',
  styleUrls: ['./updateMenuItem.scss']
})
export class UpdateMenuItemDialog {
  constructor(private dialogRef: MatDialogRef<UpdateMenuItemDialog>,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data) {
  }
  public item
  ngOnInit() {
    this.item = this.data.item
    console.log('item', this.item)
    console.log('data', this.data)
  }
 
  closeDialog() {
    this.dialogRef.close();
  }
}