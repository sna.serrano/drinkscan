import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 

// ***** Dialogs *****
import { BlankDialog } from './blankModal/blank';
import { UpdateMenuItemDialog } from './updateMenuItem/updateMenuItem';
import { UserSettingDialog } from './userSetting/userSetting';

import { SharedModule } from "../../app/shared.module";

@NgModule({
  declarations: [
    BlankDialog,
    UpdateMenuItemDialog,
    UserSettingDialog,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
  ],
  providers: [
  ],
  bootstrap: [],
  entryComponents: [
    BlankDialog,
    UpdateMenuItemDialog,
    UserSettingDialog,
  ],
  exports: [
    BlankDialog,
    UpdateMenuItemDialog,
    UserSettingDialog,
  ]
})
export class ModalModule { }
