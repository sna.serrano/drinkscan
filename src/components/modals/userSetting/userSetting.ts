import { Component, Input, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

@Component({
  selector: 'userSetting',
  templateUrl: 'userSetting.html',
  styleUrls: ['./userSetting.scss']
})
export class UserSettingDialog {
  constructor(private dialogRef: MatDialogRef<UserSettingDialog>,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data) {
  }

  closeDialog() {
    this.dialogRef.close();
  }

  firstname: boolean = false;
  lastname: boolean = false;
  password: boolean = false;
  email: boolean = false;

  user: any = {};
  passwordUpdate: any = {};

  ngOnInit() {
    this.displayDialog(this.data.type)
    this.user = this.data.user
  }

  displayDialog(type) {
    this.firstname = false
    this.lastname = false
    this.password = false
    this.email = false
    switch (type) {
      case "firstname":
        this.firstname = true
        break;
      case "lastname":
        this.lastname = true
        break;
      case "password":
        this.password = true
        break;
      case "email":
        this.email = true
        break;
    
      default:
        this.firstname = true
        break;
    }
  }
  update() {
    console.log('update user')
    // if (this.password) {
    //   this.usersService.updatePassword(this.passwordUpdate, this.user.id).subscribe((response) => {
    //     if (response.status === 200) {
    //       this.dialogRef.close();
    //       return this.snackBar.open('Mot de passe mis à jour', 'Fermer', { duration: 5000 });
    //     }
    //     else {
    //       console.log('response error', response)
    //       return this.snackBar.open('Une erreur est survenue', 'Fermer', { duration: 5000 });
    //     }
    //   })  
    // } else {
    //   this.usersService.updateAccount(this.user, this.user.id).subscribe((response) => {
    //     if (!response.error) {
    //       this.dialogRef.close();
    //       localStorage.setItem('user', JSON.stringify(response));
    //       return this.snackBar.open('Profil mis à jour', 'Fermer', { duration: 5000 });
    //     }
    //     else {
    //       console.log('response error', response)
    //       return this.snackBar.open('Une erreur est survenue', 'Fermer', { duration: 5000 });
    //     }
    //   })  
    // }
  }
  redirect() {
    this.dialogRef.close();
  }

}